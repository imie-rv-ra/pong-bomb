﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWall { 
    int ID;
    GameObject wall;

    // Start is called before the first frame update
    public PlayerWall()
    {
        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.transform.localScale += new Vector3(100, 100, 100);
        wall.transform.position = new Vector3(Random.Range(-200, 200), 1, Random.Range(-200, 200));
    }
    public PlayerWall(int id)
    {
        ID = id;
        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.GetComponent<Renderer>().material.color = new Color(Random.Range(1, 255), Random.Range(1, 255), Random.Range(1, 255));
        wall.transform.localScale += new Vector3(100, 100, 100);
        wall.transform.position = new Vector3(1, 1, -1);
    }

    // Update is called once per frame
    public void Update()
    {
       // wall.transform.Translate(new Vector3((Mathf.Sin(Time.time) * 10) * 50 * Time.deltaTime, 0.0f, (Mathf.Cos(Time.time) * 10) * 50 * Time.deltaTime));
    }

    public Vector3 getNewPos()
    {
        Vector3 pos = wall.transform.position;
        return pos +new Vector3((Mathf.Sin(Time.time) * 20) * 20 * Time.deltaTime, 0.0f, (Mathf.Cos(Time.time) * 20) * 20 * Time.deltaTime);
    }
    public GameObject GetWall()
    {
        return wall;
    }
    public int getId()
    {
        return ID;
    }
}
