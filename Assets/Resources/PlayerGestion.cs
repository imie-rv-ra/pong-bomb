﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine;


public class PlayerManager
{
    public Vector3 pos;
    public int Id = -1;
    public Vector4 quat;

   public  PlayerWall aPlayer;

    public PlayerManager(int id)
    {
        Id = id;
        aPlayer = new PlayerWall(Id); ;
    }
}

public class PlayerGestion : MonoBehaviour
{
    private GUIStyle guiStyle;
    public Font font; //set it in the inspector

    double deltaTime = 0.0f;
    double fps = 0.0f;
    double cptUpdatePosition = 0;

    bool busy = false;
    List<int> ToBeCreated = new List<int>();
    int IMustHaveToBeInstancied = -1;
    bool haveBeenCreated = false;
    BoardScript board;
    List<PlayerManager> myListe = new List<PlayerManager>();
    PlayerManager me;

    // Start is called before the first frame update
    void Start()
    {
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("en-us");
        GameObject connexion = GameObject.Find("SocketClient");
        ClientConnexion socket = connexion.GetComponent<ClientConnexion>();
        socket.AddCallBack(PlayerGestionCallBack);
        Debug.Log("Connection have been set !");

        guiStyle = new GUIStyle();
        guiStyle.font = font;
        guiStyle.normal.textColor = Color.green;
        //guiStyle.normal.textColor = Color.white;
        guiStyle.fontSize = 60;

        GameObject gm = GameObject.Find("Plane");
        board = gm.GetComponent<BoardScript>();

    }

    void OnGUI()
    {
        int yPos = 120;
        // draw the GUI button
        if (me != null)
        {
           
            GUI.Button(new Rect(50, yPos, 900, 500), "Ma pos : " + Math.Truncate(me.pos.x) + " , " + Math.Truncate(me.pos.y) + " , " + Math.Truncate(me.pos.z), guiStyle);
        }
        foreach(PlayerManager pl in myListe)
        {
            yPos += 50;
            GUI.Button(new Rect(50, yPos, 900, 500), pl.Id + " pos : " + Math.Truncate(pl.pos.x) + " , " + Math.Truncate(pl.pos.y) + " , " + Math.Truncate(pl.pos.z), guiStyle);
        }  
    }

    // Update is called once per frame
    void Update()
    {
        if(!haveBeenCreated &&  IMustHaveToBeInstancied !=-1)
        {
            me = new PlayerManager(IMustHaveToBeInstancied);
            
            board.setPlayers(myListe.Count +1);
            haveBeenCreated = true;
        }
        if (!busy && ToBeCreated.Count > 0)
        {
            foreach(int e in ToBeCreated)
            {
                Debug.Log("Client created : " + e);
                myListe.Add(new PlayerManager(e));
                
            }
            ToBeCreated.Clear();
            board.setPlayers(myListe.Count + 1);
        }

        cptUpdatePosition += Time.deltaTime;
        if (haveBeenCreated ) {
            Vector3 newPos = me.aPlayer.getNewPos();
            updatePosition(newPos.x, newPos.y, newPos.z);
            foreach(PlayerManager pl in myListe)
            {
                pl.aPlayer.GetWall().transform.position = pl.pos;
            }
            me.aPlayer.GetWall().transform.position = me.pos;
            cptUpdatePosition = 0;
        }
       
    }
    string PlayerGestionCallBack(string messageReceive)
    {
        try
        {//SetId:0
            if (messageReceive.IndexOf(':') != -1)
            {
                string functionName = messageReceive.Substring(0, messageReceive.IndexOf(':'));
                functionName= functionName.ToLower();
               // Debug.Log(messageReceive.Length);
                string args = messageReceive.Substring(messageReceive.IndexOf(':')+1 , messageReceive.Length- (messageReceive.IndexOf(':') + 1));
                args = Regex.Replace(args, "[A-Za-z].*", "");
                args = args.ToLower();

                string[] arg = args.Split(';');

              //  Debug.Log("Appel de Fonction : " + functionName + " avec " + arg.Length + " argument(s): " + args + " !");

                //SetId:int id  --> Dedier à un nouveau client, le serveur envoie l'id au nouveau client
                //updatePositions:int id,float x, float y,float z; int id,float x, float y, float z;...  //La position de tous les joueurs en jeu
                if (functionName.Equals("setid") && arg.Length ==1)
                {
                    IMustHaveToBeInstancied = int.Parse(arg[0]);
                    // updatePosition(me.GetWall().transform.position.x, me.GetWall().transform.position.y, me.GetWall().transform.position.z);
                    
                }
                else if (functionName.Equals("updatepositions")){
                    List<int> updated = new List<int>();
                    busy = true;
                  //  Debug.Log(" LES ARGS :  " + args);
                    foreach(string unArg in arg) {
                        bool trouver = false;
                       // Debug.Log(unArg);
                        string[] sousArgs = unArg.Split(',');
                        
                        if(sousArgs.Length >= 4) {
                  
                          // sousArgs[3] = Regex.Replace(sousArgs[3], "[A-Za-z].*", "");
                            //Debug.Log(" Traitement de  ID : " + sousArgs[0] + " X: " + sousArgs[1] + " Y:" + sousArgs[2] + " Z:" + sousArgs[3]);
                            foreach (PlayerManager player in myListe)
                            {
                                if (int.Parse(sousArgs[0]) == player.Id)
                                {
                                    updated.Add(player.Id);
                                    trouver = true;
                                    player.pos = new Vector3(float.Parse(sousArgs[1]), float.Parse(sousArgs[2]), float.Parse(sousArgs[3]));
                                    break;
                                }
                            }
                            if (!trouver) {
                                if(me.Id == int.Parse(sousArgs[0]))
                                {
                                    me.pos = new Vector3(float.Parse(sousArgs[1]), float.Parse(sousArgs[2]), float.Parse(sousArgs[3]));
                                    //Debug.Log("updated position : " + float.Parse(sousArgs[1]) + " , " + float.Parse(sousArgs[2]) + " , " + float.Parse(sousArgs[3]));
                                }
                                else {
                                    bool trouver2 = false;
                                    foreach (int identifiant in ToBeCreated)
                                    {
                                        if (identifiant == int.Parse(sousArgs[0]))
                                        {
                                            trouver2 = true;
                                            break;
                                        }
                                    }
                                    if (!trouver2)
                                    {
                                        updated.Add(int.Parse(sousArgs[0]));
                                        ToBeCreated.Add(int.Parse(sousArgs[0]));
                                    }
                                }                                      
                            }
                        }
                    }

/*
                    foreach (PlayerManager player in myListe)
                    {
                        bool trouver = false;
                        foreach (int ide in updated)
                        {
                            if(ide == player.Id)
                            {
                                trouver = true;
                                break;
                            }
                        }
                        if (!trouver)
                        {
                            myListe.Remove(player);
                        }
                    }*/
                busy = false;
                }
                else{
                    return "Invalide function name : " + functionName;
                }
                //Le client possèdes les functions :
                    //updatePosition: float x, float y,float z

                return "Valide !";
            }
            else
            {
                return "Invlalide data format on Player Gestion CallBack !";
            }
        }
        catch (Exception e)
        {
            return "Wrong Exception occured in Player Gestion CallBack !\n" + e;
        }
    }

    private void updatePosition(float x, float y, float z)
    {
        GameObject connexion = GameObject.Find("SocketClient");
        ClientConnexion socket = connexion.GetComponent<ClientConnexion>();
     //   socket.SendMessage("updatePosition:" + (x).ToString().Replace(',','.') + "," + (y).ToString().Replace(',', '.') + "," + (z).ToString().Replace(',', '.'));
        socket.SendMessage("updatePosition:" + x + "," + y + "," + z);
    }


}
    