﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        GameObject myConnection = GameObject.Find("SocketClient");
        ClientConnexion connexion = myConnection.GetComponent<ClientConnexion>();
        connexion.AddCallBack(CallBackExemple);
        
    }

    void Awake()
    {
        UnityThread.initUnityThread();
    }
    // Update is called once per frame
    void Update()
    {

    }

    string CallBackExemple(string messageReceive)
    {
        try { 
            Debug.Log("Iterator of , here : " + messageReceive.IndexOf(','));
            if(messageReceive.IndexOf(',') != -1)
            {
                string[] mesFloats = messageReceive.Split(',');
                Debug.Log("Number of float : " + mesFloats.Length);
                if (mesFloats.Length == 3)
                {
                    Debug.Log("float 1 : " + float.Parse(mesFloats[0]));
                    Debug.Log("float 2 : " + float.Parse(mesFloats[1]));
                    Debug.Log("float 3 : " + float.Parse(mesFloats[2]));
                    UnityThread.executeInUpdate(() =>
                    {
                        transform.Translate(new Vector3(float.Parse(mesFloats[0]), float.Parse(mesFloats[1]), float.Parse(mesFloats[2])));
                    });
                   
                    return "Success for Sphere Callback !";
                }
                else
                {
                    return "Wrong number of float for Sphere Callback !";
                }    
            }
            else
            {
                return "Wrong parameter for Sphere Callback !";
            }
        }
        catch (Exception e)
        {
            return "Wrong Exception occured in Sphere Callback !\n" + e;
        }
    }
}
