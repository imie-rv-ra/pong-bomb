﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardScript : MonoBehaviour
{
    private int players;
    public Renderer rend;

    public void setPlayers(int nb)
    {
        players = nb;
    }

    // Use this for initialization
    void Start()
    {
        players = 0;
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (players <= 2)
        {
            SetTexture("board_2players");
        } else if (players == 3)
        {
            SetTexture("board_3players");
        } else
        {
            SetTexture("board_4players");
        }
    }

    void SetTexture(string textureName)
    {
        Texture2D tex = Resources.Load(textureName, typeof(Texture2D)) as Texture2D;
        rend.material.SetTexture("_MainTex", tex);
    }
}
