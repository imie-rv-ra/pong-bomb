﻿using System.Collections;
using System.Net.Sockets;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using System.Text;
using System.Net;
using System.Globalization;

public class ClientConnexion : MonoBehaviour
{
    public delegate void DelegateDisplay(string message);
    #region private members 	
    private Socket SocketClient;
    private bool LocalsocketClientIsShutingDown;
    private byte[] readbuf;
    private byte[] sendbuf;

    public string addr = "10.35.12.51";
    public int port = 11000;

    public List<Func<string,string>> callbacksOnReceive = new List<Func<string, string>>();
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        readbuf = new byte[1024];
        ConnectToServer();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnApplicationQuit()
    {
        Close();
    }

    public void AddCallBack(Func<string, string> maFunc)
    {
        callbacksOnReceive.Add(maFunc);

    }
    /// <summary>
    /// Fonction de connection au serveur
    /// </summary>
    /// <param name="ServerName">Le nom du serveur auquel on veux se connecter</param>
    public void ConnectToServer()
    {
      //  Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");
        if (this.SocketClient == null || !this.SocketClient.Connected)
        {
            this.SocketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPAddress ipadress;
            //On récupere les informations du serveur (son nom puis son adresse IP)
            IPHostEntry he = Dns.GetHostByName(addr);
            ipadress = IPAddress.Parse(addr);

            this.SocketClient.BeginConnect(new IPEndPoint(ipadress, port), new AsyncCallback(ConnectCallback), this.SocketClient);
        }
        else
        {
            //on affiche un message
            string[] obj = new string[1] { "Déjà connecté à un serveur" };
            Debug.Log(obj);
        }

    }

    /// <summary>
    /// Fonction de rappel pour la connection
    /// </summary>
    /// <param name="asyncResult">un objet IAsyncResult</param>
    private void ConnectCallback(IAsyncResult asyncResult)
    {
        string obj;
        try
        {
            //on récupere le socket sur lequel la connexion doit avoir lieu
            Socket socket = (Socket)asyncResult.AsyncState;
            //On affect SocketClient au socket de connection
            this.SocketClient = socket;
            //on met fin à la demande de connection
            socket.EndConnect(asyncResult);

            //on affiche un message
            obj = "Connecté à un serveur";
            Debug.Log(obj);
            this.LocalsocketClientIsShutingDown = false;

            // On initie une lecture
            this.SocketClient.BeginReceive(this.readbuf, 0, this.readbuf.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), this.SocketClient);
        }
        catch (SocketException ex)
        {
            obj = ex.Message ;
          //  Debug.Log(obj);
        }
    }

    /// <summary>
    /// Fonction qui permet l'envois de message
    /// </summary>
    /// <param name="message"></param>
    public void SendMessage(string message)
    {
        if (this.SocketClient != null && this.SocketClient.Connected)
        {
            //Transformation de la string en byte pour transfert par le réseaux
            this.sendbuf = Encoding.ASCII.GetBytes(message);
            //on envois le tout.
            //Debug.Log("Envoie de message : " + message);
            this.SocketClient.BeginSend(this.sendbuf, 0, this.sendbuf.Length, SocketFlags.None, new AsyncCallback(SendCallback), this.SocketClient);
        }
        else
            DisplayMessage("Non connecté au serveur.");

    }

    /// <summary>
    /// Fonction de rappel pour la fonction d'envois de message
    /// </summary>
    /// <param name="asyncResult">Un objet IAsyncResult</param>
    private void SendCallback(IAsyncResult asyncResult)
    {
        string obj;
        try
        {
            //On récupere le socket sur lequel on a envoyé les données
            Socket socket = (Socket)asyncResult.AsyncState;
            //on met fin à l'envois de données
            int send = socket.EndSend(asyncResult);

            //ajoute le message
            obj =  "Message envoyé (" + send + " bytes envoyées )" ;
         //   Debug.Log(obj);
        }
        catch (SocketException ex)
        {
            obj = ex.Message ;
            Debug.Log(obj);
        }
    }

    /// <summary>
    /// Fonction qui lance l'attente de message
    /// </summary>
    public void ReceiveMessage()
    {

        if (this.SocketClient != null && this.SocketClient.Connected)
            this.SocketClient.BeginReceive(this.readbuf, 0, this.readbuf.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), this.SocketClient);
        else
            DisplayMessage("Non connecté au serveur.");

    }

    /// <summary>
    /// Fonction de rappel pour la réception d'un message
    /// </summary>
    /// <param name="asyncResult">Un objet IAsyncResult transmit par BeginReceive</param>
    private void ReceiveCallback(IAsyncResult asyncResult)
    {
        string obj;
        if (this.SocketClient != null && this.SocketClient.Connected)
        {
            try
            {
                //On récupere le socket de la connection
                Socket socket = (Socket)asyncResult.AsyncState;

                //on lis les données sur le socket
                int read = socket.EndReceive(asyncResult);

                //si on lis plus de 0 octet alors c'est que tout le message a été transmis
                if (read > 0)
                {
                    //ajoute le message
                    obj =Encoding.ASCII.GetString(this.readbuf);
                    //Debug.Log(obj);
                    // reset buffer
                    Buffer.SetByte(this.readbuf, 0, 0);
                    // On initie une lecture
                    this.SocketClient.BeginReceive(this.readbuf, 0, this.readbuf.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), this.SocketClient);
                    DisplayMessage(obj);
                }
                //Si 0 octets est transmis alors on doit fermer la connection
                if (read == 0 && !this.LocalsocketClientIsShutingDown)
                {
                    this.SocketClient.Close();
                    obj ="Fermeture socket distante" ;
                    Debug.Log(obj);
                }
            }
            catch (SocketException ex)
            {
                obj =  ex.Message;
               // Debug.Log(obj);
            }
        }
    }

    /// <summary>
    /// Fonction qui ferme correctement la communication
    /// </summary>
    public void Close()
    {
        if (this.SocketClient != null && this.SocketClient.Connected)
        {
            this.LocalsocketClientIsShutingDown = true;
            //On ferme le socket
            this.SocketClient.Shutdown(SocketShutdown.Both);
            //System.Threading.Thread.Sleep(1000);
            //On détruit le socket
            this.SocketClient.Close();
            //On ajoute le message à la listbox .
            string obj = "Connexion fermée" ;
            Debug.Log(obj);
        }
    }

    /// <summary>
    /// Fonction qui permet l'ajout d'un message dans la listbox dans le bon thread
    /// </summary>
    /// <param name="message">Le message à afficher</param>
    private void DisplayMessage(string message)
    {
        message = message.Substring(0, message.IndexOf('\0'));
        message = message.Trim();
        //this.listBox.SelectedIndex = this.listBox.Items.Count - 1;
    //   Debug.Log("MEssage reçu du serveur : " + message);
      //  Debug.Log("Taille de la liste a CallBack : " + callbacksOnReceive.Count);
        //ICI Code callback pour gerer l'arrivage des données
        foreach (Func<string,string> function in callbacksOnReceive){
            function(message); //ici on dit que la fonction de callback doit renvoyer une String, 
            //Basiquement la sring permet de savoir si dans les logs un events a été validé ou non par un gameobject et si oui lequel
        }
    }
}

