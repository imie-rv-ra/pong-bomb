﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ImageScript : MonoBehaviour
{
    public Vector3 _startPosition;

    float speed = 50;
    float amplitude = 4;

    Vector2 origin = new Vector2(0.0f, 0.0f);

    private ImageTargetBehaviour mImageTargetBehaviour = null;

    // Start is called before the first frame update
    void Start()
    {
        // We retrieve the ImageTargetBehaviour component
        // Note: This only works if this script is attached to an ImageTarget
        mImageTargetBehaviour = GetComponent<ImageTargetBehaviour>();

        

        if (mImageTargetBehaviour == null)
        {
            Debug.Log("ImageTargetBehaviour not found ");
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (mImageTargetBehaviour == null)
        {
           
            return;
        }
        else
        {
            Vector2 targetSize = mImageTargetBehaviour.GetSize();
            float targetAspect = targetSize.x / targetSize.y;
            // We define a point in the target local reference 
            // we take the bottom-left corner of the target, 
            // just as an example
            // Note: the target reference plane in Unity is X-Z, 
            // while Y is the normal direction to the target plane
            Vector3 pointOnTarget = new Vector3(-0.5f, 0, -0.5f / targetAspect);
            // We convert the local point to world coordinates
            Vector3 targetPointInWorldRef = transform.TransformPoint(pointOnTarget);
            // We project the world coordinates to screen coords (pixels)
            Vector3 screenPoint = Camera.main.WorldToScreenPoint(targetPointInWorldRef);
            Vector2 targetPoint = new Vector2(Vector3.Normalize(screenPoint).x, Vector3.Normalize(screenPoint).y);
           Debug.Log("target point in screen coords: " + targetPoint.x + ", " + targetPoint.y);
            origin = targetPoint;
        }




        //Debug.Log("Angle du markeur : " + Mathf.Acos(Vector3.Dot(origin, screenPoint)  /  (Vector3.Magnitude(origin) * Vector3.Magnitude(screenPoint))).ToString()   );
      //   Debug.Log("Angle du markeur : " + Vector2.Dot(targetPoint, origin).ToString()    );
       
        //Debug.LogError((Mathf.Sin(Time.time)).ToString());
        // transform.Translate(new Vector3((Mathf.Sin(Time.time) * amplitude) * speed * Time.deltaTime, 0.0f, (Mathf.Cos(Time.time) * amplitude) * speed * Time.deltaTime));

        // Console.WriteLine();
        //transform.position = _startPosition;// + new Vector3(Mathf.Sin(Time.time), 0.0f, 0.0f);
    }
}
