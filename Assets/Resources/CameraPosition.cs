﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    private Rect mButtonRect = new Rect(50, 50, 900, 500);
    public Font font; //set it in the inspector
    private GUIStyle guiStyle;
    // Start is called before the first frame update
    void Start()
    {
        //   GUILayout.BeginArea(new Rect(50, 50, 900, 500), "Camera pos : " + Math.Truncate(this.transform.position.x) + " , " + Math.Truncate(this.transform.position.y) + " , " + Math.Truncate(this.transform.position.z));
        //in awake or start use these lines of code to set the size of the font
        guiStyle = new GUIStyle();
        guiStyle.font = font;
        guiStyle.normal.textColor = Color.green;
        //guiStyle.normal.textColor = Color.white;
        guiStyle.fontSize = 60;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnGUI()
    {   
        // draw the GUI button
        if (GUI.Button(mButtonRect, "Camera pos : " + Math.Truncate( this.transform.position.x) + " , " + Math.Truncate(this.transform.position.y) + " , " + Math.Truncate(this.transform.position.z), guiStyle)) ;
        {
            // do something on button click	
        }
    }
}
